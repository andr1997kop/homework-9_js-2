"use strict"

/*
1. Типи даних: number, string, boolean, undefined, null, symbol, BigInt,
object.
2. == нестроге порівняння, тобто порівняння значень без врахування типу даних
=== строге порівняння, тобто якщо порівнювати однакові дані але різного типу,
то буде false.
3. Оператор це символ, що дозволяє виконати певну логічну, арифметичну операцію.
Також використовується при конкатенації, зміну типу даних (як унарний +). = присвоює
змінній значення.
*/

let name = prompt("Enter your name");
let age = prompt("Enter your age");


while (name == null) {
    name = prompt("Enter your name", name);
}

while (age == null || isNaN(age) || age <= 0) {
    age = prompt("Enter your age", age);
}

if (age < 18){
    alert(`You are not allowed to visit this website`)
}

else if (age >=18 && age <=22){
    
    if (confirm(`Are you sure you want to continue?`)){
        alert (`Welcome, ${name}`)
    }
    else {
        alert(`You are not allowed to visit this website`)
    }

}

else if (age > 22){
    alert (`Welcome, ${name}`)
}



